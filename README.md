## Goban - Play GO

This is a React.js - Redux.js project. It is a Goban usable to play a game of GO. It is built with functional programming paragdim in mind and is as "functional" as it can be using JS and React. All the functions are pures (apart from I/O operations obvioulsy), all variables are constants and there is no reassigning inisde arrays/objects and for obvious reasons all the loops are written in a recursive fashion. 

The code style is checked using eslint-plugin-fp (on top pf others rules) to check that any of the best practices and rules in the functional programming paradigm are coorectly followed. 

It is styled using `styled-components` library.

In the project directory, you can run:

### `npm install` 
and then 
### `npm start`
