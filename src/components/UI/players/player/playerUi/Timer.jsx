import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';

const StyledTimer = styled.div`
  h3{
    font-size: 1.9rem;
    font-weight: unset;
  }
`;

function Timer(props) {
  const { timer } = props;
  const minutes = Math.floor((timer / 100) / 60);
  const seconds = Math.floor((timer / 100) % 60);
  const minutesDisplay = minutes.toString().length > 1 ? minutes : `0${minutes}`;
  const secondsDisplay = seconds.toString().length > 1 ? seconds : `0${seconds}`;

  return (
    <StyledTimer>
      <h3>Time:</h3>
      <p>
        {minutesDisplay}
        :
        {secondsDisplay}
      </p>
    </StyledTimer>
  );
}

function mapStateToProps() {
  return {

  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Timer);
