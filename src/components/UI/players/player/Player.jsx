import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import Timer from './playerUi/Timer';

const StyledPlayer = styled.div`
  border: 2px solid black;
  border-radius: 20px;
  height: 500px;
  width: 200px;
  font-size: 1.6rem;
  font-weight: normal;
  text-align: center;
  h2{
    font-size: 2.2rem;
  }
  .captured{
    
  }
  .message {
    color: red;
  }
`;

function Player(props) {
  const {
    color, captured, timer, turn, message,
  } = props;
  const capturedStones = color === 'black' ? captured[0] : captured[1];
  return (
    <StyledPlayer>
      <h2>{color.charAt(0).toUpperCase() + color.slice(1)}</h2>
      <p className="captured">
        Captured:
        {capturedStones}
      </p>
      <Timer timer={timer} />
      <p className="message">{turn === color ? message : null}</p>
    </StyledPlayer>
  );
}

function mapStateToProps(state) {
  return {
    captured: state.captured,
    turn: state.turn,
    message: state.message,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Player);
