import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import Player from './player/Player';

const StyledPlayers = styled.div`
    display:flex;
    justify-content: space-between;
    padding: 0 50px;
    height: ${`${window.innerHeight}px`};
    align-items: center;
`;

function Players(props) {
  const { timers } = props;
  return (
    <StyledPlayers>
      <Player color="white" timer={timers[0]} />
      <Player color="black" timer={timers[1]} />
    </StyledPlayers>
  );
}

function mapStateToProps(state) {
  return {
    timers: state.timers,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({

  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Players);
