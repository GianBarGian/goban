import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';

const StyledMessage = styled.div`
    position:absolute;
    bottom: 0;
    left:46%;
    right: 50%;
    p{
      width: 200px;
      font-size: 1.4rem;
    }
`;

function Message(props) {
  const { turn } = props;

  return (
    <StyledMessage>
      <p>
        {turn ? ` ${turn.toUpperCase()} TURN` : 'GAME OVER'}
      </p>
    </StyledMessage>
  );
}

function mapStateToProps(state) {
  return {
    turn: state.turn,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Message);
