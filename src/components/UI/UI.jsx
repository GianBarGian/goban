import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import Message from './message/Message';
import Players from './players/Players';

const StyledUI = styled.div`
    position:relative;
    height: 100%;
`;

function UI() {
  return (
    <StyledUI>
      <Players />
      <Message />
    </StyledUI>
  );
}

function mapStateToProps() {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UI);
