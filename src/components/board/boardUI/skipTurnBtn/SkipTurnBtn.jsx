import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';

import {
  skipTurn, endGame, startGame, createBoard,
} from '../../../../states/actionCreators';

const StyledButton = styled.button`
    margin: 0 auto;
    position: relative;
    top: 30px;
    z-index: 1;
`;

function SkipTurnBtn(props) {
  const {
    history, turn, skipTurn, endGame, startGame,
  } = props;

  const skip = () => {
    const lastMove = history[history.length - 1];
    return lastMove && lastMove.coordinates === 'skipped'
      ? endGame()
      : skipTurn(turn);
  };

  const start = () => startGame();

  if (turn) {
    return (
      <StyledButton onClick={skip}>Skip Turn</StyledButton>
    );
  }
  return (
    <StyledButton onClick={start}>Start Game</StyledButton>
  );
}

function mapStateToProps(state) {
  return {
    history: state.history,
    turn: state.turn,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    skipTurn,
    createBoard,
    startGame,
    endGame,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SkipTurnBtn);
