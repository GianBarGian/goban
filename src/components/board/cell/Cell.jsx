import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import Stone from './stone/Stone';

const StyledCell = styled.div`
    padding: 20px 0;
    position: relative;
    width: 5.01%;

    margin-top: ${({ y, size }) => {
    const topCell = y === size - 1;
    return topCell ? '-14px' : 0;
  }};

    margin-bottom: ${({ y }) => {
    const botCell = y === 0;
    return botCell ? '-10px' : 0;
  }};
`;

const StyledBorder = styled.span`
    position: absolute;
    width: 100%;
    height: 100%;
    top: -17px;
    left: 0;

    border-bottom: ${({ x, size }) => {
    const rightCell = x === size - 1;
    return rightCell ? 0 : '2px solid black;';
  }};

    border-left: ${({ y, size }) => {
    const topCell = y === size - 1;
    return topCell ? 0 : '2px solid black;';
  }};

`;

const StyledDot = styled.span`
    position: absolute;
    z-index: 1;
    font-size: 70px;
    left: -10px;
    top: -25px;
    border: 0;
`;

function Cell(props) {
  const { cell, size } = props;
  const x = cell.coordinates[0];
  const y = cell.coordinates[1];


  const createDots = (x, y) => {
    if (
      (size > 12
        // Corner dots
        && ((x === size - 4 && y === size - 4)
          || (x === 3 && y === 3)
          || (x === size - 4 && y === 3)
          || (x === 3 && y === size - 4)))
      // Side dots
      || (size === 19
        && ((x === 3 && y === 9)
          || (x === size - 4 && y === 9)
          || (x === 9 && y === 3)
          || (x === 9 && y === size - 4)))
      // Center dot
      || (x === Math.floor(size / 2)
        && y === Math.floor(size / 2))) {
      return <StyledDot>.</StyledDot>;
    } return null;
  };

  return (
    <StyledCell y={y} size={size}>
      <StyledBorder x={x} y={y} size={size}>
        {createDots(x, y)}
        <Stone cell={cell} x={x} y={y} />
      </StyledBorder>
    </StyledCell>
  );
}

function mapStateToProps(state) {
  return {
    size: state.size,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({

  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Cell);
