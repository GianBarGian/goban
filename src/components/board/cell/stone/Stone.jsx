import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { nextTurn, updateMessage } from '../../../../states/actionCreators';
import { getNeighbours } from '../../helpers';

const StyledStone = styled.div`
    position: absolute;
    padding: 17px;
    width: 7%;
    border-radius: 50%;
    top: 24px;
    left: -19px;
    z-index: 2;

    background-color: ${(props) => {
    if (props.cell.owner === 'black') {
      return 'black';
    }
    if (props.cell.owner === 'white') {
      return 'white';
    }
    if (props.turn === 'black') {
      return 'black';
    }
    if (props.turn === 'white') {
      return 'white';
    }
    return 'none';
  }};
    
    opacity: ${props => (props.cell.owner === 'empty' ? 0 : 1)};

    :hover {
        opacity: ${props => (props.cell.owner !== 'empty' ? 1 : 0.5)}
    }
`;

function Stone(props) {
  const {
    board, cell, turn, nextTurn, x, y, size, prevCaptured, updateMessage,
  } = props;

  const resetKo = (board, player = turn) => {
    const newBoard = board.map(column => column.map((cell) => {
      if (cell.ko === player) {
        const tempCell = { ...cell, ko: false };
        return tempCell;
      }
      return cell;
    }));
    return newBoard;
  };

  const checkCapture = (stone, board, checkedStones = [], stonesToCheck = []) => {
    const x = stone.coordinates[0];
    const y = stone.coordinates[1];
    const neighbours = getNeighbours(x, y, size, board);
    const stoneHasLiberty = neighbours.some(neighbour => neighbour.owner === 'empty');
    const captured = { ...stone, owner: 'empty' };
    const accChecked = checkedStones.concat(captured);
    const toCheck = stonesToCheck.concat(neighbours).filter(item => (
      accChecked.every(stone => stone.coordinates !== item.coordinates)
      && stone.owner === item.owner));
    const accToCheck = toCheck.slice(toCheck[toCheck.length - 1]);

    if (stoneHasLiberty) return false;
    if (toCheck.length) {
      const result = checkCapture(toCheck[toCheck.length - 1], board, accChecked, accToCheck);
      return result && !result.includes(false) ? result : false;
    }
    if (accChecked.length === 1) {
      const koStone = [{ ...accChecked[0], ko: stone.owner }];
      return koStone;
    }
    return accChecked;
  };

  const checkNeighbours = (board) => {
    const neighbours = getNeighbours(x, y, size, board);
    const enemyStones = neighbours.filter(neighbour => neighbour.owner !== 'empty' && neighbour.owner !== turn);
    const checkedStones = enemyStones.length
      && enemyStones.map(stone => checkCapture(stone, board));
    const captured = checkedStones && checkedStones.filter(stone => stone);
    return captured.length ? captured : false;
  };

  const updateBoard = (capturedStones = {}) => {
    const newCell = { ...cell, owner: turn };
    const updatedBoard = JSON.parse(JSON.stringify(board));
    updatedBoard[size - y - 1][x] = newCell;

    return capturedStones.length
      ? capturedStones.flat().reduce((acc, stone) => {
        acc[size - stone.coordinates[1] - 1][stone.coordinates[0]] = stone;
        return acc;
      }, updatedBoard)
      : updatedBoard;
  };

  const playStone = () => {
    if (turn) {
      const updatedBoard = resetKo(updateBoard());
      const newCell = { ...cell, owner: turn };
      const isSuicidal = checkCapture(newCell, updatedBoard);
      const isCaptured = checkNeighbours(updatedBoard);
      const capturedReal = isCaptured
        && Array.from(new Set(isCaptured.flat().map(JSON.stringify))).map(JSON.parse);
      const capturedBoard = resetKo(updateBoard(capturedReal));
      const capturedNum = isCaptured && capturedReal.flat().length;
      const capturedState = turn === 'black' ? [capturedNum + prevCaptured[0], prevCaptured[1]] : [prevCaptured[0], capturedNum + prevCaptured[1]];
      if (isSuicidal && !isCaptured) return updateMessage('Suicide is not allowed!');
      if (cell.ko) return updateMessage('KO! Need a turn to capture this stone.');
      if (cell.owner !== 'empty') return null;
      if (!isCaptured) return nextTurn(updatedBoard, newCell, prevCaptured);

      if (capturedReal.length > 1) {
        const player = capturedReal[0].ko;
        const doubleKoBoard = resetKo(capturedBoard, player);
        return nextTurn(doubleKoBoard, newCell, capturedState);
      }

      return nextTurn(capturedBoard, newCell, capturedState);
    }
    return null;
  };

  return (
    <StyledStone turn={turn} cell={cell} onClick={playStone} />
  );
}

function mapStateToProps(state) {
  return {
    board: state.board,
    turn: state.turn,
    size: state.size,
    prevCaptured: state.captured,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    nextTurn,
    updateMessage,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Stone);
