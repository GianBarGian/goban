import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';

import { createBoard, updateTimers, endGame } from '../../states/actionCreators';
import Cell from './cell/Cell';
import SkipTurnBtn from './boardUI/skipTurnBtn/SkipTurnBtn';

const StyledBoard = styled.div`
    display: flex;
    flex-wrap: wrap;
    max-width: 800px;
    width: 100%;
    left: 0;
    right: 0;
    margin: 20px auto;
    background-color: #efd58a;
    box-sizing: border-box;
    justify-content: flex-end;
    position: absolute;
    padding-top: 15px;
    border-radius: 4%;
`;


function Board(props) {
  const {
    size, board, turn, timers, createBoard, updateTimers, endGame, gameOver,
  } = props;

  useEffect(() => {
    if (!board.length) createBoard(size, size);
    if (turn) {
      const newTimers = turn === 'black' ? [timers[0], timers[1] - 1] : [timers[0] - 1, timers[1]];
      setTimeout(() => updateTimers(newTimers), 10);
    }
  }, [timers]);

  useEffect(() => {
    if (gameOver) endGame();
  }, [gameOver]);

  return (
    <StyledBoard>
      {
        board.map(column => column.map(cell => <Cell cell={cell} key={cell.coordinates} />))
      }
      <SkipTurnBtn />
    </StyledBoard>
  );
}

function mapStateToProps(state) {
  return {
    size: state.size,
    board: state.board,
    turn: state.turn,
    timers: state.timers,
    gameOver: state.gameOver,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createBoard,
    updateTimers,
    endGame,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Board);
