function createCells(size, y, accumulator = -1, rows = []) {
  const index = accumulator + 1;
  if (index === size) return rows;
  const cell = {
    coordinates: [index, y],
    owner: 'empty',
    ko: false,
  };
  const result = rows.concat(cell);
  return createCells(size, y, index, result);
}

export function initBoard(accumulator, size, rows = []) {
  const index = accumulator - 1;
  if (index < 0) return rows;
  const result = rows.concat([createCells(size, index)]);
  return initBoard(index, size, result);
}


export function getNeighbours(x, y, size, board) {
  const left = board[size - y - 1][x - 1] ? board[size - y - 1][x - 1] : false;
  const bottom = board[size - y] ? board[size - y][x] : false;
  const right = board[size - y - 1][x + 1] ? board[size - y - 1][x + 1] : false;
  const top = board[size - y - 2] ? board[size - y - 2][x] : false;

  return [top, right, bottom, left].filter(stone => stone);
}
