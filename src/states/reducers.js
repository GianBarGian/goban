import * as types from './actionTypes';

export function size(state = 19, action) {
  switch (action.type) {
    case types.CHANGE_SIZE:
      return action.payload.size;
    default:
      return state;
  }
}

export function board(state = [], action) {
  switch (action.type) {
    case types.CREATE_BOARD:
      return action.payload.board;
    case types.NEXT_TURN:
      return action.payload.board;
    case types.RESET_CAPTURED:
      return action.payload.board;
    case types.START_GAME:
      return [];
    default:
      return state;
  }
}

export function turn(state = false, action) {
  switch (action.type) {
    case types.START_GAME:
      return 'black';
    case types.NEXT_TURN:
      return state === 'black' ? 'white' : 'black';
    case types.SKIP_TURN:
      return state === 'black' ? 'white' : 'black';
    case types.END_GAME:
      return false;
    default:
      return state;
  }
}

export function history(state = [], action) {
  switch (action.type) {
    case types.NEXT_TURN:
      return [...state, action.payload.move];
    case types.SKIP_TURN:
      return [...state, action.payload.move];
    default:
      return state;
  }
}

export function gameOver(state = true, action) {
  switch (action.type) {
    case types.START_GAME:
      return false;
    case types.END_GAME:
      return true;
    default:
      return state;
  }
}


export function captured(state = [0, 0], action) {
  switch (action.type) {
    case types.START_GAME:
      return [0, 0];
    case types.NEXT_TURN:
      return action.payload.captured;
    default:
      return state;
  }
}

export function timers(state = [0, 0], action) {
  switch (action.type) {
    case types.START_GAME:
      return [180000, 180000];
    case types.UPDATE_TIMERS:
      return action.payload.timers;
    case types.END_GAME:
      return [0, 0];
    default:
      return state;
  }
}

export function message(state = '', action) {
  switch (action.type) {
    case types.UPDATE_MESSAGE:
      return action.payload.message;
    case types.NEXT_TURN:
      return '';
    default:
      return state;
  }
}
