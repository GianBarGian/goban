import * as types from './actionTypes';
import { initBoard } from '../components/board/helpers';


export const createBoard = size => ({
  type: types.CREATE_BOARD,
  payload: {
    board: initBoard(size, size),
  },
});

export const nextTurn = (board, stone, captured) => ({
  type: types.NEXT_TURN,
  payload: {
    move: { coordinates: stone.coordinates, turn: stone.owner },
    board,
    captured,
  },
});


export const skipTurn = turn => ({
  type: types.SKIP_TURN,
  payload: {
    move: { coordinates: 'skipped', turn },
  },
});

export const startGame = () => ({
  type: types.START_GAME,
});

export const endGame = () => ({
  type: types.END_GAME,
});

export const updateTimers = timers => ({
  type: types.UPDATE_TIMERS,
  payload: {
    timers,
  },
});

export const updateMessage = message => ({
  type: types.UPDATE_MESSAGE,
  payload: {
    message,
  },
});
