import React from 'react';
import styled from 'styled-components';

import Board from './components/board/Board';
import UI from './components/UI/UI';

import marmo from './assets/marmo.jpg';

const StyledApp = styled.div`
  background-image: url(${marmo});
  height: ${`${window.innerHeight}px`};
`;

export default function App() {
  return (
    <StyledApp className="App">
      <Board />
      <UI />
    </StyledApp>
  );
}
