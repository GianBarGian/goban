import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {
  legacy_createStore as createStore, compose, applyMiddleware, combineReducers,
} from 'redux';
import thunk from 'redux-thunk';

import './index.css';
import App from './App';
import * as reducers from './states/reducers';

const rootReducer = combineReducers({
  size: reducers.size,
  board: reducers.board,
  turn: reducers.turn,
  history: reducers.history,
  gameOver: reducers.gameOver,
  captured: reducers.captured,
  timers: reducers.timers,
  message: reducers.message,
});

const store = createStore(
  rootReducer,
  {},
  compose(
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunk),
  ),
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
